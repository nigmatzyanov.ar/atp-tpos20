from flask import Flask, request, render_template, Response
from flask_sqlalchemy import SQLAlchemy

import prometheus_client
from prometheus_client.core import CollectorRegistry
from prometheus_client import Counter

import logging.config
from settings import logger_config
logging.config.dictConfig(logger_config)
logger = logging.getLogger('web_logger')

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///metrics.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class Metrics(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	metric = db.Column(db.String(200), nullable = False)

	def __init__(self, metric):
		self.metric = metric


graphs = {}
graphs['c'] = Counter('python_request', 'The total number of processed requests')  # возьмем только Counter


def shutdown_server():
	func = request.environ.get('werkzeug.server.shutdown')
	if func is None:
		raise RuntimeError('Not running with the Werkzeug Server')
	func()



@app.route('/')
def main():
	graphs['c'].inc()
	for k, v in graphs.items():
		new_metric = Metrics(metric = prometheus_client.generate_latest(v))
		db.session.add(new_metric)
		db.session.commit()
	headers = request.headers
	return render_template("index.html", headers = headers)


@app.route("/metrics")
def requests_count():
	res = []
	for k, v in graphs.items():
		res.append(prometheus_client.generate_latest(v))
		
	return Response(res, mimetype = "text/plain")
	
@app.route('/shutdown', methods=['GET'])
def shutdown():
	shutdown_server()
	return 'Server shutting down...'
	
if __name__ == '__main__':
	db.create_all()
	app.run(host = "0.0.0.0", port = 8444, debug = True)

