import logging, sys

class OutFilter(logging.Filter):
    def filter(self, sss):
        return sss.levelno in (logging.DEBUG, logging.INFO)

logger_config = {
    'version': 1,
    'disable_existing_loggers': False,

    'formatters': {
        'serr_formatter': {
            'format': '{asctime} - {name} - STDERR:{levelname} - {message}',
            'style': '{'
        },
        
        'sout_formatter': {
            'format': '{asctime} - {name} - STDOUT:{levelname} - {message}',
            'style': '{'
        }
    },
    
    'handlers': {
        'serr': {
            'class': 'logging.StreamHandler' ,
            'stream': 'ext://sys.stderr',
            'level': 'WARNING',
            'formatter': 'serr_formatter'
        },
        
        'sout': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
            'level': 'DEBUG',
            'formatter': 'sout_formatter',
            'filters': ['tpos_filter']
        },
        
    },
    
    'loggers': {
        'web_logger': {
            'level': 'DEBUG',
            'handlers': ['sout', 'serr']
        }
    },
    
    'filters': {
        'tpos_filter': {
            '()': OutFilter   
        }
     },
}
